# New Adventures
# Copyright (C) 2021  Laskar Alexander Theuer

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


### Needed Libraries ###
from os import system, name
import time
import random
### New Generation Libraries ###
from Kampfsystem import Battle  # USAGE: Battle.MobName; Battle.MobHP, Battle.MobDMG, PlyrDMG, Battle.PlyrHP, Battle.PlyrPotions


### Player Stats ###
BaseDmg = 2
WeapDmg = 0.5
PlyrDMG = int(BaseDmg) + float(WeapDmg)
PlyrHP = 15



### Player Data ###
NoraSocial = False
LeoSocial = None
MarkusSocial = None
NoraUndisturbed = True
DylanUndisturbed = True
NicoUndisturbed = True
KimAlive = None



### Unlocking the Evil Ending ###
malevolence = 0
indifference = None



### Needed functions ###
def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')

def EndSec():
    input("*Drücke Enter zum fortfahren*")
    clear()



### The Actual Game ###
if __name__ == "__main__":
    # Set Playername
    AskName = open("Playerdata/AskForPlayername", "r")
    Name = input(AskName.read())
    Playername = open("Playerdata/Name", "w")
    Playername.write(Name)
    Playername.close()
    clear()
    time.sleep(0.5)

    # Print Prologue
    Playername = open("Playerdata/Name", "r")
    print(f"Willkommen, {Playername.read()} + Bevor du anfängst, lass dir erklären, worum es geht.\nASTOLFO")
    time.sleep(0.5)
    Prologue = open("Chapters/Prologue", "r")
    print(Prologue.read())
    EndSec()
   
    # Print First Section of First Chapter
    Chap1Sec1 = open("Chapters/Chap1/1", "r")
    print(Chap1Sec1.read())
    Choice1 = input("Angreifen (1) oder Abwarten (2)\n")
    if Choice1 == "1":
        clear()
        Sel1 = open("Options/Chapters/1/1a", "r")
        print(Sel1.read())
        exit()
        # Death
    elif Choice1 == "2":
        clear()
        Sel1a = open("Options/Chapters/1/1b1", "r")
        Sel1b = open("Options/Chapters/1/1b2", "r")
        print(Sel1a.read())
        Playername = open("Playerdata/Name", "r")
        print(f"Mein Name ist {Playername.read()} Und wer bist du?\n {Sel1b.read()}")
        EndSec()
        # Proceed with next Section

    # Print Second Section of the first Chapter
    Enemy1 = Battle("Verlorener Wahnsinniger", 13, 2, 2.5, 3, PlyrDMG, PlyrHP, 3, True, "Schlag", "Stossangriff", "Kopfstoss")
    Bat1 = open("Chapters/Chap1/2a", "r")
    print(Bat1.read() + "\n")
    EndSec()

    # Fighting Time!
    while Enemy1.Fight == True:
        Bat1Dec = input("Was möchten sie tun?\nAngreifen (1) oder sich heilen (2)?\n")
        
        if Bat1Dec == "1":
            Enemy1.Attack(Enemy1.MobName, Enemy1.MobHP, Enemy1.PlyrDMG, PlyrHP)
        elif Bat1Dec == "2":
            Enemy1.Heal(Enemy1.PlyrHP, Enemy1.PlyrPotions, Enemy1.MobName)
        else:
            print("Bitte gib beim nächsten mal einen der numerischen Werte ein.")

        Enemy1.LoseLifes()

    # After a won battle, print the rest of this section
    Bat2 = open("Chapters/Chap1/2b", "r")
    print(Bat2.read() + "\n")
    EndSec()
    PlyrHP=Enemy1.PlyrHP

    # Print 3rd Section of First Chapter
    Chap1Sec3a = open("Chapters/Chap1/3a", "r")
    print(Chap1Sec3a.read() + "\n")
    EndSec()
