from os import system
import time
from random import randint as rng


class Battle:
    # Startup and Stats and other Fight Data
    def __init__(self, MobName, MobHP, MobDMG1, MobDMG2, MobDMG3, PlyrDMG, PlyrHP, PlyrPotions, Fight, MobAtt1, MobAtt2, MobAtt3):
        self.MobName = MobName
        self.MobHP = MobHP
        self.MobDMG1 = MobDMG1
        self.MobDMG2 = MobDMG2
        self.MobDMG3 = MobDMG3
        self.PlyrDMG = PlyrDMG
        self.PlyrHP = PlyrHP
        self.PlyrPotions = PlyrPotions
        self.Fight = Fight
        self.MobAtt1 = MobAtt1
        self.MobAtt2 = MobAtt2
        self.MobAtt3 = MobAtt3

    # Fighting!
    def Attack(self, MobName, MobHP, PlyrDMG, PlyrHP):
        if MobHP > 0:
            if self.PlyrHP > 0:
                print(MobName + "\n")
                print(f"Das Monster hat {str(self.MobHP)} Leben!")
                input("*Drücke Enter um fortzufahren*")
                system("clear")
                self.MobHP = float(self.MobHP) - float(PlyrDMG)
                print("Du greifst an!")
                time.sleep(1)
                system("clear")
                print(MobName + "\n")
                print(f"Das Monster hat nach dem Angriff noch {str(self.MobHP)} Leben!")
                input("*Drücke Enter um fortzufahren*")
                system("clear")
            else:
                self.Fight = False
        else:
            print("Sie haben das Monster besiegt!")
            self.Fight = False
    
    def MobAttack1(self, MobName, MobDMG1, PlyrHP, MobAtt1, MobHP):
        if self.PlyrHP > 0:
            if self.MobHP > 0 and self.Fight == True:
                print(MobName + "\n")
                print(f"Deine Leben: {str(self.PlyrHP)}")
                input("*Drücke Enter um fortzufahren*")
                system("clear")
                print(f"Das Monster führt einen {MobAtt1} aus!")
                time.sleep(1)
                system("clear")
                self.PlyrHP = float(self.PlyrHP) - float(self.MobDMG1)
                print(MobName + "\n")
                print(f"Deine Leben sind jetzt: {str(self.PlyrHP)}")
                input("*Drücke Enter um fortzufahren*")
                system("clear")
            else:
                PlyrHP = self.PlyrHP
                self.Fight = False
        else:
            self.Fight = False
            print("Sie sind gestorben!")
            exit()

    def MobAttack2(self, MobName, MobDMG2, PlyrHP, MobAtt2, MobHP):
        if self.PlyrHP > 0:
            if self.MobHP > 0 and self.Fight == True:
                print(MobName + "\n")
                print("Deine Leben: " + str(self.PlyrHP))
                input("*Drücke Enter um fortzufahren*")
                system("clear")
                print(f"Das Monster führt einen {MobAtt2} aus!")
                time.sleep(1)
                system("clear")
                self.PlyrHP = float(self.PlyrHP) - float(self.MobDMG2)
                print(MobName + "\n")
                print(f"Deine Leben sind jetzt: {str(self.PlyrHP)}")
                input("*Drücke Enter um fortzufahren*")
                system("clear")
            else:
                PlyrHP = self.PlyrHP
                self.Fight = False
        else:
            self.Fight = False
            print("Sie sind gestorben!")
            exit()

    def MobAttack3(self, MobName, MobDMG3, PlyrHP, MobAtt3, MobHP):
        if self.PlyrHP > 0:
            if self.MobHP > 0 and self.Fight == True:
                print(MobName + "\n")
                print("Deine Leben: " + str(self.PlyrHP))
                input("*Drücke Enter um fortzufahren*")
                system("clear")
                print(f"Das Monster führt einen {MobAtt3} aus!")
                time.sleep(1)
                system("clear")
                self.PlyrHP = float(self.PlyrHP) - float(self.MobDMG3)
                print(MobName + "\n")
                print(f"Deine Leben sind jetzt: {str(self.PlyrHP)}")
                input("*Drücke Enter um fortzufahren*")
                system("clear")
            else:
                PlyrHP = self.PlyrHP
                self.Fight = False
        else:
            self.Fight = False
            print("Sie sind gestorben!")
            exit()

    def LoseLifes(self):
        MobAttack = rng(1, 3)
        if MobAttack == 1:
            self.MobAttack1(self.MobName, self.MobDMG1, self.PlyrHP, self.MobAtt1, self.MobHP)
        elif MobAttack == 2:
            self.MobAttack2(self.MobName, self.MobDMG2, self.PlyrHP, self.MobAtt2, self.MobHP)
        elif MobAttack == 3:
            self.MobAttack3(self.MobName, self.MobDMG3, self.PlyrHP, self.MobAtt3, self.MobHP)

    def Heal(self, PlyrHP, PlyrPotions, MobName):
        if self.PlyrPotions > 0:
            print(self.MobName + "\n")
            Potions = int(self.PlyrPotions) - 1
            print(f"Du hast noch {str(self.PlyrPotions)} Heiltränke\n")
            self.PlyrHP = float(self.PlyrHP) + 2.5
            print(f"Du hast wieder {str(PlyrHP)} Leben\n")
            input("*Drücke Enter um fortzufahren*")
            system("clear")
        else:
            print("Du hast leider keine Heiltränke!")
